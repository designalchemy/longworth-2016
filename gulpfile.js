'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var fileinclude = require('gulp-file-include');
var markdown = require('markdown');
var browserSync = require('browser-sync').create();
var autoprefixer = require('gulp-autoprefixer');
var notify = require('gulp-notify');
var concat = require('gulp-concat');

gulp.task('fileinclude', function(){

  gulp.src(['src/*.html'])
      .pipe(fileinclude({
        prefix: '@@',
        basepath: '@file',
        filters: {
          markdown: markdown.parse
        }
      }))
      .pipe(gulp.dest('./dist'))
      .pipe( notify({ message: "fileInclude tasks have been completed!"}) );
});

gulp.task('browser-sync', function () {

    browserSync.init({
        // server: {
        //   baseDir: './dist'
        // },
        proxy: 'http://192.168.10.10/'
    });

});

gulp.task('sass', function () {
  gulp.src('./scss/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(gulp.dest('./public/wp-content/themes/longworth/css'))
    .pipe(browserSync.stream())
    .pipe( notify({ message: "SASS tasks have been completed!"}) );
});

gulp.task('scripts', function(){
  return gulp.src('./js/*.js')
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest('./public/wp-content/themes/longworth/js'))
    .pipe( notify({ message: "JS/Coffee tasks have been completed!"}) );
});

gulp.task('watch', ['fileinclude'], function () {
  gulp.watch('scss/**/*.scss', ['sass']);
  gulp.watch('src/*.html', ['fileinclude']);
  gulp.watch('src/*.md', ['fileinclude']);
  gulp.watch('./js/*.js', ['scripts'])
  gulp.watch('dist/*.html').on('change', browserSync.reload);
});

gulp.task('default', ['scripts', 'sass', 'fileinclude', 'browser-sync', 'watch']);