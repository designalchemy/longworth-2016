<?php
/**
 * Longworth functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Longworth
 */

if ( ! function_exists( 'longworth_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function longworth_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Longworth, use a find and replace
	 * to change 'longworth' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'longworth', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'longworth' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'longworth_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'longworth_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function longworth_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'longworth_content_width', 640 );
}
add_action( 'after_setup_theme', 'longworth_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function longworth_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'longworth' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'longworth' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'longworth_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function longworth_scripts() {
	wp_enqueue_style( 'longworth-style', get_stylesheet_uri() );

	wp_enqueue_script( 'longworth-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'longworth-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'longworth_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

add_theme_support('post-thumbnails');

// Our custom post type function
function create_posttype() {

	register_post_type( 'projects',
	// CPT Options
		array(
			'labels' 			=> array(
				'name' 			=> __( 'Projects' ),
				'singular_name' => __( 'Project' )
			),
			'public' 			=> true,
			'has_archive' 		=> true,
			'rewrite' 			=> array('slug' => 'projects'),
			'supports'          => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', '', '', '', ),
			'menu_icon'   		=> 'dashicons-clipboard',
			'taxonomies' 		=> array('post_tag')
		)
	);

	register_post_type( 'products',
	// CPT Options
		array(
			'labels' 			=> array(
				'name' 			=> __( 'Products' ),
				'singular_name' => __( 'product' )
			),
			'public' 			=> true,
			'has_archive' 		=> true,
			'rewrite' 			=> array('slug' => 'products'),
			'supports'			=> array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'tags', '', '', ),
			'menu_icon'   		=> 'dashicons-cart',
			'taxonomies' 		=> array('post_tag')
		)
	);

	register_post_type( 'Services',
	// CPT Options
		array(
			'labels' 			=> array(
				'name' 			=> __( 'Services' ),
				'singular_name' => __( 'service' )
			),
			'public' 			=> true,
			'has_archive' 		=> true,
			'rewrite' 			=> array('slug' => 'services'),
			'supports'           => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'tags', '', '', ),
			'menu_icon'   		=> 'dashicons-admin-tools',
			'taxonomies' 		=> array('post_tag')
		)
	);

	register_post_type( 'News',
	// CPT Options
		array(
			'labels' 			=> array(
				'name'			=> __( 'News' ),
				'singular_name' => __( 'news_item' )
			),
			'public' 			=> true,
			'has_archive' 		=> true,
			'rewrite'			=> array('slug' => 'news'),
			'supports'          => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'tags', '', '', ),
			'menu_icon'   		=> 'dashicons-welcome-view-site',
			'taxonomies' 		=> array('post_tag')
		)
	);

	register_post_type( 'people',
	// CPT Options
		array(
			'labels' 			=> array(
				'name' 			=> __( 'People' ),
				'singular_name' => __( 'person' )
			),
			'public' 			=> true,
			'has_archive' 		=> true,
			'rewrite' 			=> array('slug' => 'people'),
			'supports' 			=> array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'tags', '', '', ),
			'menu_icon'   		=> 'dashicons-universal-access-alt',
			'taxonomies' 		=> array('post_tag')
		)
	);

	register_post_type( 'sectors',
	// CPT Options
		array(
			'labels' 			=> array(
				'name' 			=> __( 'Sectors' ),
				'singular_name' => __( 'sector' )
			),
			'public' 			=> true,
			'has_archive' 		=> true,
			'rewrite' 			=> array('slug' => 'sectors'),
			'supports' 			=> array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'tags', '', '', ),
			'menu_icon'   		=> 'dashicons-welcome-widgets-menus',
			'taxonomies' 		=> array('post_tag')
		)
	);

}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );

// THIS GIVES US SOME OPTIONS FOR STYLING THE ADMIN AREA
function custom_colors() {
   echo '<style type="text/css">
           #menu-posts-projects,
           #menu-posts-products,
           #menu-posts-services,
           #menu-posts-news,
           #menu-posts-people,
           #menu-posts-sectors {
           	background: black !important;
           }
         </style>';
}

add_action('admin_head', 'custom_colors');



function remove_menus(){
  


  //remove_menu_page( 'edit.php' );                   //Posts
  remove_menu_page( 'edit-comments.php' );          //Comments
  remove_menu_page( 'themes.php' );                 //Appearance
  remove_menu_page( 'users.php' );                  //Users


  
}
add_action( 'admin_menu', 'remove_menus' );

