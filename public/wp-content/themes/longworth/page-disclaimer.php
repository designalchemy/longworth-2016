<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Longworth
 */

get_header(); ?>

<?php
if (have_posts()) {
	while (have_posts()) {
			the_post();
?>

<section class="cream-background">

    <div class="container">

        <h1 class="fatty fade-out-scoll"><?php the_title(); ?></h1>

    </div>

</section>

<section>

    <div class="container">

        <h1 class="fade-in-on-scroll">The following applys to all our customers</h1>

        <div class="howard-split-big empty-first">

            <div class="mobile-hide">

                &nbsp;

            </div>

            <div>

            	<?php the_content(); ?>

            </div>

        </div>

    </div>

</section>

<?php

	} // end while
} // end if

get_footer();
