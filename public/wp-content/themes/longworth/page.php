<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Longworth
 */

get_header(); ?>

<?php
if (have_posts()) {
	while (have_posts()) {
			the_post();
?>

<section class="big-background parallax-window" data-parallax="scroll" data-image-src="<?php echo the_post_thumbnail_url(); ?>">

    <!-- <img src="img/background/leafy.png"> -->

    <div class="container">

        <h1 class="fatty fade-out-scoll"><?php the_title(); ?></h1>

    </div>

</section>

<section class="cream-background small-padding">

    <div class="container">

        <div class="howard-split-big empty-first">

            <div class="mobile-hide">&nbsp; </div>

            <div class="text-split-two-col">

                <?php the_content(); ?>

            </div>

        </div>

    </div>

</section>

<section>

    <div class="container">

        <?php if (get_field('first_section_title')) { ?>
        <h1 class="fade-in-on-scroll" data-delay="0"><?php the_field('first_section_title'); ?></h1>
        <?php } ?>

        <?php if (get_field('first_section_oversize_text')) { ?>
        <h2 class="cream-font fade-in-on-scroll" data-delay="0"><?php the_field('first_section_oversize_text'); ?></h2>
        <?php } ?>

        <div class="howard-split-big empty-first">

            <div class="mobile-hide">&nbsp; </div>

            <div>

                <ul class="small-list text-split-two-col fade-in-on-scroll">

                    <?php

                    // check if the repeater field has rows of data
                    if (have_rows('first_section_blurb')):

                     	// loop through the rows of data
                        while (have_rows('first_section_blurb')) : the_row();
                    ?>
                    	<li><?php the_sub_field('first_blurb_item'); ?></li>

                    <?php

                        endwhile;

                    else :

                        // no rows found

                    endif;

                    ?>

                </ul>

            </div>

        </div>

    </div>

</section>

<section class="big-background parallax-window" data-parallax="scroll" data-image-src="<?php echo the_post_thumbnail_url(); ?>">

    <!-- <img src="img/background/leafy.png"> -->

</section>

<section>

    <div class="container">

        <?php if (get_field('second_section_title')) { ?>
        <h1 class="fade-in-on-scroll" data-delay="0"><?php the_field('second_section_title'); ?></h1>
        <?php } ?>

        <?php if (get_field('second_section_oversize_text')) { ?>
        <h2 class="cream-font fade-in-on-scroll" data-delay="0"><?php the_field('second_section_oversize_text'); ?></h2>
        <?php } ?>

        <div class="howard-split-big empty-first">

            <div class="mobile-hide">&nbsp; </div>

            <div>

                <ul class="small-list text-split-two-col fade-in-on-scroll">

                    <?php

                    // check if the repeater field has rows of data
                    if (have_rows('second_section_blurb')):

                     	// loop through the rows of data
                        while (have_rows('second_section_blurb')) : the_row();
                    ?>
                    	<li><?php the_sub_field('section_blurb_item'); ?></li>

                    <?php

                        endwhile;

                    else :

                        // no rows found

                    endif;

                    ?>

                </ul>

            </div>

        </div>

    </div>

</section>

<?php

	} // end while
} // end if

get_footer();
