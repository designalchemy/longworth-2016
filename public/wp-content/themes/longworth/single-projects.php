<?php get_header(); ?>

<?php
if (have_posts()) {
	while (have_posts()) {
			the_post();
?>

<section class="big-background parallax-window" data-parallax="scroll" data-image-src="<?php echo the_post_thumbnail_url(); ?>">

    <div class="container">

        <h1 class="fatty fade-out-scoll"><?php the_title(); ?></h1>

        <h1 class="fade-out-scoll white-text"><?php the_field('sub_title') ?></h1>

    </div>

</section>

<section class="cream-background small-padding" id="project-details">

    <div class="container">

    	<div class="project-details-split">

    		<div class="fade-in-on-scroll">

    			<h1 class="white-text">Architect</h1>

    			<p><?php the_field('architect') ?></p>

    		</div>

    		<div class="fade-in-on-scroll" data-delay="200">
    			<h1 class="white-text">Value</h1>

    			<p><?php the_field('value') ?></p>

    		</div>

    		<div class="fade-in-on-scroll" data-delay="400">
    			<h1 class="white-text">Contractor</h1>

    			<p><?php the_field('contractor') ?></p>

    		</div>

    	</div>

    </div>

</section>

<section class="light-cream-background" id="project-detail">

    <div class="container">

	    <h2 class="fade-in-on-scroll" data-delay="0"><?php the_field('blurb_title') ?></h2>

	    <div class="howard-split-big empty-first">

	        <div class="mobile-hide">&nbsp; </div>

	        <div class="fade-in-on-scroll text-split-two-col" data-delay="200">

	        	<?php the_content(); ?>

	        </div>

	    </div>

    </div>

</section>

<?php

$images = get_field('images');

if ($images):

?>

    <section class="small-padding over-flow">

        <div class="container">

    	    <ul class="bxslider fade-in-on-scroll">

                <?php foreach ($images as $image): ?>

                    <li><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></li>

                <?php endforeach; ?>

    	    </ul>

        </div>

    </section>

<?php endif; ?>

<?php

// check if the repeater field has rows of data
if (have_rows('specification_list')):

?>

    <section class="light-cream-background">

        <div class="container">

            <div class="howard-split-big">

                <div class="">

                	<h1 class="fade-in-on-scroll" data-delay="0">Specification</h1>

               	</div>

                <div class="text-split-two-col fade-in-on-scroll" data-delay="200">

                    <ul class="small-list dark-text">

                        <?php

                        // loop through the rows of data
                        while (have_rows('specification_list')) : the_row();

                        ?>
                        	<li><?php the_sub_field('specification'); ?></li>

                        <?php

                        endwhile;

                        ?>

                    </ul>

                </div>

            </div>

        </div>

    </section>

<?php

endif;

?>

<section class="dark-cream-background" id="project-team">

    <div class="container">

    	<div class="project-team-split">

    		<div>

    			<h1 class="fade-in-on-scroll">Team Members involved</h1>

                <?php

                $posts = get_field('members');
                $i = 0;
                $delay = 0;

                if ($posts):

                    foreach ($posts as $post): // variable must be called $post (IMPORTANT)

                        $i++;
                        $delay += 100;

                        if ($i > 3) {
                            wp_reset_postdata();
                            break;
                        }

                        ?>
                        <?php setup_postdata($post); ?>

                        <a href="<?php echo get_permalink(); ?>" class="fade-in-on-scroll" data-delay="<?php $delay ?>">

                            <img src="<?php echo the_post_thumbnail_url(); ?>" alt="">

                            <div class="team-member-cell">

                                <p><?php the_title(); ?></p>

                                <hr class="small-hr">

                                <p><?php the_field('job_title') ?></p>

                            </div>

                        </a>

                    <?php endforeach; ?>

                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

                <?php endif; ?>

    		</div>

    		<div>

    			<div>

    				<h1 class="fade-in-on-scroll" data-delay="200">Products Used</h1>

                    <?php

                    $posts = get_field('products');

                    if ($posts): ?>

                        <?php foreach ($posts as $post): // variable must be called $post (IMPORTANT) ?>

                            <?php setup_postdata($post); ?>

                            <a href="<?php echo get_permalink(); ?>" class="fade-in-on-scroll"><?php the_title(); ?></a>

                        <?php endforeach; ?>

                        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

                    <?php endif; ?>

    			</div>

    			<div>

    				<h1 class="fade-in-on-scroll" data-delay="400">Tags</h1>

                    <?php echo get_the_tag_list(); ?>

    			</div>

    		</div>

    	</div>

    </div>

</section>

<?php

	} // end while
} // end if

get_footer();

