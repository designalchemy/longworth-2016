<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>

    <head>

    	<meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Longworth</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel='shortcut icon' href='favicon.ico' type='image/x-icon'>
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <link rel="stylesheet" href="<?php echo  get_template_directory_uri (); ?>/css/style.css">
        <link rel="stylesheet" href="<?php echo  get_template_directory_uri (); ?>/css/font-awesome.min.css">

        <link rel="apple-touch-icon" sizes="57x57" href="/fav/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/fav/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/fav/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/fav/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/fav/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/fav/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/fav/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/fav/apple-touch-icon-180x180.png">
        <link rel="icon" type="image/png" href="/fav/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="/fav/android-chrome-192x192.png" sizes="192x192">
        <link rel="icon" type="image/png" href="/fav/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="/fav/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="/fav/manifest.json">
        <link rel="mask-icon" href="/fav/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="msapplication-TileImage" content="/fav/mstile-144x144.png">

        <meta name="theme-color" content="#0b5f86">

    </head>

    <body <?php body_class(); ?>>

        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <?php get_template_part( 'template-parts/nav', 'none' ); ?>

        <?php get_template_part( 'template-parts/sub-nav', 'none' ); ?>

        <main class="main-body">

            <div class="filter"></div>

            <div class="loader-container">

                <div class='uil-squares-css'>
                <div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div></div>

            </div>
