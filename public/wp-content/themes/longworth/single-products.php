<?php get_header(); ?>

<?php
if (have_posts()) {
	while (have_posts()) {
			the_post();
?>

<section class="big-background parallax-window" data-parallax="scroll" data-image-src="<?php echo the_post_thumbnail_url(); ?>">

	<div class="blue-on-top-filter"></div>

    <div class="container">

        <h1 class="fatty fade-out-scoll"><?php the_title(); ?></h1>

    </div>

</section>

<section class="cream-background small-padding">

    <div class="container">

        <div class="howard-split-big empty-first">

            <div class="mobile-hide">&nbsp; </div>

            <div class="text-split-two-col">

                <?php the_content(); ?>

            </div>

        </div>

    </div>

</section>

<section class="" id="people-projects">

	<div class="container">

		<div class="smaller-break-col">

			<div>

				<h1>Material</h1>

			</div>

			<div>

				<h1>Manufacturer</h1>

			</div>

		</div>

		<?php

		// check if the repeater field has rows of data
		if (have_rows('project_materials')):

		 	// loop through the rows of data
		    while (have_rows('project_materials')) : the_row();

			?>

				<div class="smaller-break-col">

		        <div>

		        	<ul class="small-list">

		        		<li><?php the_sub_field('project_material'); ?></li>

		        	</ul>

		        </div>

		        <div>

			        <?php

			        // check if the repeater field has rows of data
			        if (have_rows('project_manufacture')):

			         	// loop through the rows of data
			            while (have_rows('project_manufacture')) : the_row();

			        	?>

			        		<?php if (the_sub_field('project_manufacture_link')) { ?>

				             	<a href="<?php the_sub_field('project_manufacture_link'); ?>"><?php the_sub_field('project_manufacture_name'); ?></a><br>

				            <?php

				       		} else {

				       		?>

				       			<p class="no-margin-bottom"><?php the_sub_field('project_manufacture_name'); ?></p>

			       			<?php

			       			}

			            endwhile;

			        else :

			            // no rows found

			        endif;

			        ?>

		        </div>

		        </div>

		        <?php

		    endwhile;

		else :

		    // no rows found

		endif;

		?>

	</div>

</section>

<section class="dark-gray-background" id="people-projects">

    <div class="container">

    	<h1 class="white-text">Product related projects</h1>

    	<div class="two-col project-split">

    	<?php

    	$posts = get_field('projects');

    	if ($posts): ?>

    	    <?php foreach($posts as $post): // variable must be called $post (IMPORTANT) ?>
    	        <?php setup_postdata($post); ?>

    	        <a href="<?php echo get_permalink(); ?>" class="feature-big-sub-split fade-in-on-scroll" style="background: url('<?php echo the_post_thumbnail_url(); ?>');">

    	            <div class="blue-filter">
    	            </div>

    	            <div class="text-cell">

    	                <h3><?php the_title(); ?></h3>

    	                <div>

    	                    <p>Client:</p>

    	                    <p><?php the_field('sub_title') ?></p>

    	                    <p>Value:</p>

    	                    <p><?php the_field('value') ?></p>

    	                </div>

    	            </div>

    	        </a>

    	    <?php endforeach; ?>

    	    <?php wp_reset_postdata(); ?>

    	<?php endif; ?>

    	</div>

    </div>

</section>

<?php

	} // end while

} // end if

get_footer();

