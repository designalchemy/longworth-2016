<?php get_header(); ?>

<section class="" id="">

	<div class="container">

		<h1 class="dark-text big-font">Tags</h1>

		<div class="two-col big-right-margin">

			    <a href="<?php echo get_permalink(); ?>">

			    	<div class="people-details">

			    		<h3><?php the_title(); ?></h3>

			    		<p><?php the_excerpt(); ?></p>

			    	</div>

			    </a>

		</div>

	</div>

</section>


<?php
wp_reset_postdata(); // reset to the original page data

get_footer();

