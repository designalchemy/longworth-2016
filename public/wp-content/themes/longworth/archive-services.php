<?php get_header(); ?>

<section class="" id="">

	<div class="container">

		<h1 class="dark-text big-font">News</h1>

		<div class="two-col">

			<?php

				$args = array('post_type' => 'services', 'posts_per_page' => '-1');

				$loop = new WP_Query($args);

				if ($loop->have_posts()) {

				  while ($loop->have_posts()) {

				    $loop->the_post();

			?>

			    <a href="<?php echo get_permalink(); ?>">

			    	<img src="<?php echo the_post_thumbnail_url(); ?>" alt="">

			    	<div class="people-details">

			    		<h3><?php the_title(); ?></h3>

			    		<p class="cream-font"><?php the_date('d M Y'); ?></p>

			    		<p><?php the_excerpt(); ?></p>

			    	</div>

			    </a>

			<?php

				}

			}

			?>

		</div>

	</div>

</section>

<?php
wp_reset_postdata(); // reset to the original page data

get_footer();
