<?php get_header(); ?>

<?php
if (have_posts()) {
	while (have_posts()) {
			the_post();
?>

<section class="cream-background">

    <div class="container">

        <h1 class="fatty fade-out-scoll"><?php the_title(); ?></h1>

    </div>

</section>

<section>

    <div class="container">

	    <h2 class="fade-in-on-scroll" data-delay="0"><?php the_field('blurb_title') ?></h2>

	    <div class="howard-split-big empty-first">

	        <div class="mobile-hide">&nbsp; </div>

	        <div class="fade-in-on-scroll" data-delay="200">

	        	<?php the_content(); ?>

	        </div>

	    </div>

    </div>

</section>

<?php

	} // end while

} // end if

get_footer();
