<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Longworth
 */

get_header(); ?>

<?php
if (have_posts()) {
    while (have_posts()) {
            the_post();
?>

<section class="big-background parallax-window" data-parallax="scroll" data-image-src="<?php echo the_post_thumbnail_url(); ?>">

    <!-- <img src="img/background/leafy.png"> -->

    <div class="container">

        <h1 class="fatty fade-out-scoll"><?php the_title(); ?></h1>

    </div>

</section>

<section class="cream-background small-padding">

    <div class="container">

        <div class="howard-split-big empty-first">

            <div class="mobile-hide">&nbsp; </div>

            <div class="text-split-two-col">

                <?php the_content(); ?>

            </div>

        </div>

    </div>

</section>

<section>

    <div class="container gallery-container">

        <?php 

        $images = get_field('awards');

        if( $images ): ?>

                <?php foreach( $images as $image ): ?>

                    <div>

                        <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />

                    </div>

                <?php endforeach; ?>

        <?php endif; ?>

    </div>

</section>

<?php

    } // end while
} // end if

get_footer();
