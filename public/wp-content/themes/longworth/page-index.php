<?php get_header(); ?>

<section id="home-slider">

    <div class="slider-container">

        <div class="slider-counter">

            <div class="container"></div>

        </div>

        <div class="slider-text-container">

            <div class="container">

                <!-- mirror 3rd slide replace with js mirroing on load -->

                <div>
                    <h1 class="first-text-target fatty">1st Uk leading ...</h1>
                </div>

                <div>
                    <h1 class="second-text-target fatty">contractors</h1>
                </div>

            </div>

        </div>

        <div class="slider-cell">

            <a href="#">

                <img src="<?php echo get_template_directory_uri (); ?>/img/home-slider/carousel1.jpg">

                <h1>1st Uk leading ...</h1>

                <h1>contractors</h1>

            </a>

        </div>

        <div class="slider-cell">

            <a href="#">

                <img src="<?php echo get_template_directory_uri (); ?>/img/home-slider/carousel2.jpg">

                <h1>Lorem ipsum dolor </h1>

                <h1>sit amet</h1>

            </a>

        </div>

        <div class="slider-cell">

            <a href="#">

                <img src="<?php echo get_template_directory_uri (); ?>/img/home-slider/carousel3.jpg">

                <h1>Lorem ipsum dolor </h1>

                <h1>sit amet</h1>

            </a>

        </div>

        <div class="slider-cell">

            <a href="#">

                <img src="<?php echo get_template_directory_uri (); ?>/img/home-slider/carousel4.jpg">

                <h1>Lorem ipsum dolor </h1>

                <h1>sit amet</h1>

            </a>

        </div>


    </div>

</section>

<section id="home-services">

    <div class="container">

        <h1 class="fade-in-on-scroll">Services</h1>

        <h2 class="fade-in-on-scroll">Offering complete turnkey services — design, installation and project management.</h2>

        <div class="howard-split-big">

            <div class="mobile-hide">

                &nbsp;

            </div>

            <div class="three-col">

            <?php

            $args = array( 'post_type' => 'services' );
            $loop = new WP_Query( $args );
            $delay = 0;

            if ($loop->have_posts()) {

              while ($loop->have_posts()) {

                $loop->the_post();
                $delay += 100;

                ?>

                <div class="global-image-cell fade-in-on-scroll" data-delay="<?php echo $delay ?>" href="<?php echo get_permalink(); ?>">

                	<div class="image">
                		<img src="<?php the_field('icon') ?>" alt="">
                	</div>

                	<span>
                		<h4><?php the_title(); ?></h4>

                		<p><?php the_excerpt() ?></p>
                	</span>

                </div>

            <?php

            	}

            }

            wp_reset_postdata(); // reset to the original page data

            ?>

            </div>

        </div>

    </div>

</section>

<section id="home-featured">

    <div class="container">

        <h1 class="fade-in-on-scroll">Featured projects</h1>

        <div class="home-project-split">

        <?php

        $args = array( 'post_type' => 'projects' );
        $loop = new WP_Query( $args );
        $delay = 0;

        if ($loop->have_posts()) {

          while ($loop->have_posts()) {

            $loop->the_post();
            $delay += 100;

            ?>

            <a href="<?php echo get_permalink(); ?>" data-delay="<?php echo $delay ?>" class="fade-in-on-scroll" style="background: url('<?php echo the_post_thumbnail_url(); ?>');">

                <div class="blue-filter">
                </div>

                <div class="text-cell">

                    <h3><?php the_title(); ?></h3>

                    <div>

                        <p>Client:</p>

                        <p><?php the_field('sub_title'); ?></p>

                        <p>Value:</p>

                        <p><?php the_field('value'); ?></p>

                    </div>

                </div>

            </a>

        <?php

            }

        }

        wp_reset_postdata(); // reset to the original page data

        ?>

    </div>

</section>

<section id="home-next-project">

    <div class="container">

        <h1 class="fade-in-on-scroll">Want to talk about your next project?</h1>

        <a href="/contact" class="fade-in-on-scroll"><img src="<?php echo get_template_directory_uri (); ?>/img/small-right-arrow.png"> Drop us a line</a>

    </div>

</section>

<section id="home-products">

    <div class="container">

        <h1 class="fade-in-on-scroll">Products</h1>

        <h2 class="fade-in-on-scroll" >We've got them all covered.</h2>

        <div class="howard-split-big">

	        <div class="mobile-hide">

	            &nbsp;

	        </div>

            <div class="three-col">

	            <?php

	            $args = array('post_type' => 'products');
	            $loop = new WP_Query($args);
                $bottomDelay = 0;

	            if ($loop->have_posts()) {

	              while ($loop->have_posts()) {

	                $loop->the_post();
                    $bottomDelay += 100;

	                ?>

	                <a href="<?php echo get_permalink(); ?>" class="fade-in-on-scroll" data-delay="<?php echo $bottomDelay ?>"><?php the_title(); ?></a>

	            <?php

	            	}

	            }

	            wp_reset_postdata(); // reset to the original page data

	            ?>

            </div>

        </div>

    </div>

    </section>

    <section id="home-news">

        <div class="container">

            <div class="two-col">

                <div>

                    <h1 class="fade-in-on-scroll">Latest news</h1>

                    <div class="two-col news-cell">

                        <?php

                        $args = array('post_type' => 'news', 'posts_per_page' => 2);
                        $loop = new WP_Query($args);
                        $bottomDelay = 0;

                        if ($loop->have_posts()) {

                          while ($loop->have_posts()) {

                            $loop->the_post();
                            $bottomDelay += 200;

                            ?>

                            <a href="<?php echo get_permalink(); ?>" class="fade-in-on-scroll" data-delay="<?php echo $bottomDelay ?>">

                                <h3><?php the_title(); ?></h3>

                                <p class="date"><?php echo get_the_date('d M Y'); ?></p>

                                <p class="mobile-hide"><?php the_excerpt(); ?></p>

                            </a>

                        <?php

                            }

                        }

                        wp_reset_postdata(); // reset to the original page data

                        ?>

                    </div>

                </div>

                <div>

                    <h1 class="fade-in-on-scroll" data-delay="400">Instagram</h1>

                    <div class="three-col insta-target">

                        <a href="#" class="fade-in-on-scroll" data-delay="400">

                            <div class="blue-filter">
                                <img src="<?php echo get_template_directory_uri (); ?>/img/insta-1.png" alt="">
                            </div>

                        </a>

                        <a href="#" class="fade-in-on-scroll" data-delay="600">

                            <div class="blue-filter">
                                <img src="<?php echo get_template_directory_uri (); ?>/img/insta-2.png" alt="">
                            </div>

                        </a>

                        <a href="#" class="fade-in-on-scroll last" data-delay="800">

                            <div class="blue-filter">
                                <img src="<?php echo get_template_directory_uri (); ?>/img/insta-3.png" alt="">
                            </div>

                        </a>

                    </div>

                </div>

             </div>

        </div>

    </section>

    <section id="home-quote">

        <div class="container">

            <h1 class="fade-in-on-scroll">What our clients say</h1>

            <blockquote cite="http://example.com/facts" class="fade-in-on-scroll">
                "Quis justo sed nulla ornare scelerisque  gravttda bridante. Sed pulvinar lacus eu  porta interdum dui felis vehicula elit...”

                <p>Persons Name — Company Name</p>
            </blockquote>

        </div>

    </section>

</section>

<?php

get_footer();
