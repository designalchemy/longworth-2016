<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Longworth
 */

get_header(); ?>

<?php
if (have_posts()) {
	while (have_posts()) {
			the_post();
?>

<section class="cream-background">

    <!-- <img src="img/background/leafy.png"> -->

    <div class="fade-out-scoll container">

    	<h1 class="dark-text"><?php echo get_the_date('d M Y'); ?></h1>

        <h2 class="dark-text"><?php the_title(); ?></h2>

    </div>

</section>

<section class="light-cream-background">

    <div class="container">

	    <div class="howard-split-big empty-first">

	        <div class="mobile-hide">&nbsp; </div>

	        <div class="fade-in-on-scroll">

	        	<?php the_content(); ?>

	        </div>

	    </div>

    </div>

</section>

<?php

	} // end while

} // end if

get_footer();

