<?php get_header(); ?>

<?php
if (have_posts()) {
	while (have_posts()) {
			the_post();
?>

    <section class="cream-background big-background people-cover-photo parallax-window" data-position="0 0" data-parallax="scroll" data-image-src="<?php echo the_field('cover_photo'); ?>">

        <div class="container">

            <h1 class="fatty fade-out-scoll"><?php the_title(); ?></h1>

            <h1 class="fade-out-scoll white-text"><?php the_field('job_title') ?></h1>

        </div>

    </section>

    <section class="cream-background">

        <div class="container">

            <div>
            &nbsp; 
            </div>

    	    <div class="howard-split-big empty-first">

    	        <div class="mobile-hide">&nbsp; </div>

    	        <div class="fade-in-on-scroll text-split-two-col">

    	        	<?php the_content(); ?>

    	        </div>

    	    </div>

        </div>

    </section>

    <?php if (have_rows('bio')): ?>

        <section class="people-bio">

            <?php while (have_rows('bio')) : the_row(); ?>

                <div class="container">

                    <div class="howard-split-big">

                        <div class="">
                            <h1><?php the_sub_field('title_of_section') ?></h1>
                        </div>

                        <div class="fade-in-on-scroll">

                            <ul class="small-list dark-text">

                                <?php

                                if (have_rows('bio_items')):

                                while (have_rows('bio_items')) : the_row();

                                ?>

                                    <li><?php the_sub_field('bio_item'); ?></li>


                                <?php

                                endwhile;

                                endif;

                                ?>

                            </ul>

                        </div>

                    </div>

                </div>

            <?php

            endwhile;

            ?>

        </section>

    <?php endif; ?>

    <?php

    $members = get_posts(array(
        'post_type' => 'projects',
        'meta_query' => array(
            array(
                'key' => 'members', // name of custom field
                'value' => '"' . get_the_ID() . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
                'compare' => 'LIKE'
            )
        )
    ));

    $posts = get_field('projects');

    if ($members): ?>

        <section class="dark-gray-background" id="people-projects">

            <div class="container">

            	<h1 class="white-text">Projects <?php the_title(); ?> has worked on</h1>

            	<div class="two-col project-split">

            	    <?php foreach($members as $member): // variable must be called $post (IMPORTANT) ?>

            	        <?php setup_postdata($member); ?>

            	        <a href="<?php echo get_permalink($member->ID); ?>" class="feature-big-sub-split fade-in-on-scroll" style="background: url('<?php echo get_the_post_thumbnail_url($member->ID); ?>');">

            	            <div class="blue-filter">
            	            </div>

            	            <div class="text-cell">

            	                <h3><?php echo get_the_title($member->ID); ?></h3>

            	                <div>

            	                    <p>Client:</p>

            	                    <p><?php echo get_field('sub_title', $member->ID) ?></p>

            	                    <p>Value:</p>

            	                    <p><?php echo get_field('value', $member->ID) ?></p>

            	                </div>

            	            </div>

            	        </a>

            	    <?php endforeach; ?>

            	    <?php wp_reset_postdata(); ?>

            	</div>

            </div>

        </section>

    <?php endif; ?>

<?php

	} // end while

} // end if

get_footer();
