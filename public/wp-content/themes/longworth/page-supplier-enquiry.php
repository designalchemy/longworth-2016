<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Longworth
 */

get_header(); ?>

<?php
if (have_posts()) {
	while (have_posts()) {
			the_post();
?>

<section class="cream-background">

    <div class="container">

        <h1 class="fatty fade-out-scoll"><?php the_title(); ?></h1>

    </div>

</section>

<section>

    <div class="container">

        <h2 class="cream-font fade-in-on-scroll" data-delay="0"><?php echo get_the_content(); ?></h2>

        <div class="howard-split-big empty-first">

            <div class="mobile-hide">&nbsp; </div>

            <div class="two-col margin-bottom">

            	<div>

	            	<h1>Head Office</h1>

	            	<address>

	            		North Florida Road<br>
	            		Haydock Industrial Estate<br>
	            		Haydock<br>
	            		St Helens<br>
	            		WA11 9UB

	            	</address>

	            	<a href="#">Google Maps</a>

            	</div>

            	<div>

	            	<h1>Edinburgh Office</h1>

	            	<address>

		            	10 Lochside Place<br>
		            	Edinburgh<br>
		            	EH12 9RG

	            	</address>

	            	<a href="#">Google Maps</a>

            	</div>

            </div>

        </div>

        <div class="howard-split-big empty-first">

            <div class="mobile-hide">&nbsp; </div>

            <div class="contact-details">

            	<div>

            		<h1>Telephone</h1>
            		<h1 class="cream-font"><a href="#">0845 634 1370</a></h1>

        		</div>

				<div>

					<h1>Fax</h1>
					<h1 class="cream-font"><a href="#">0845 634 1379</a></h1>

				</div>

				<div>

					<h1>Email</h1>
					<h1 class="cream-font"><a href="#">info@longworth-uk.com</a></h1>

				</div>


            </div>

        </div>

    </div>

</section>




<?php

	} // end while
} // end if

get_footer();
