<?php get_header(); ?>

<section class="cream-background ">

	<div class="container">

	<h2 class="fade-in-on-scroll">All projects</h2>

    	<div class="two-col project-split">

		<?php

			$args = array('post_type' => 'projects', 'posts_per_page' => '-1');
			$loop = new WP_Query( $args );

			if ($loop->have_posts()) {

			  while ($loop->have_posts()) {

			    $loop->the_post();
			    /* loop markup here using the_content() etc */

		?>

			<a href="<?php echo get_permalink(); ?>" class="feature-big-sub-split fade-in-on-scroll" style="background: url('<?php echo the_post_thumbnail_url(); ?>');">

			    <div class="blue-filter">
			    </div>

			    <div class="text-cell">

			        <h3><?php the_title(); ?></h3>

			        <div>

			            <p>Client:</p>

			            <p><?php the_field('sub_title') ?></p>

			            <p>Value:</p>

			            <p><?php the_field('value') ?></p>

			        </div>

			    </div>

			</a>

		<?php

			}

		}

		?>

    	</div>

	</div>

</section>

<?php
wp_reset_postdata(); // reset to the original page data

get_footer();

