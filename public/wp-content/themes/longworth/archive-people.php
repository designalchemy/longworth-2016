<?php get_header(); ?>

<section class="cream-background" id="people">

	<div class="container">

	<h2>Meet the team</h2>

		<div class="flex-four-col">

		<?php

			$args = array('post_type' => 'people', 'posts_per_page' => '-1');

			$loop = new WP_Query($args);

			if ($loop->have_posts()) {

			  while ($loop->have_posts()) {

			    $loop->the_post();

		?>

		    <a href="<?php echo get_permalink(); ?>">

		    	<div class="image-crop">
		    		<img src="<?php echo the_post_thumbnail_url(); ?>" alt="">
		    	</div>

		    	<div class="people-details">

		    		<h3><?php the_title(); ?></h3>

		    		<hr class="small-hr">

		    		<p><?php the_field('job_title') ?></p>

		    	</div>

		    </a>

		<?php

			}

		}

		?>

		</div>

	</div>

</section>


<?php
wp_reset_postdata(); // reset to the original page data

get_footer();

