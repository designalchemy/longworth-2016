<div class="nav-target">

	<div class="close js-close-nav fa fa-times fa-1x"></div>

	<div id="about-us" class="nav-content">

		<p class="nav-slide">We are one of the UK’s leading envelope contractors, offering a complete turnkey service of design, installation and project management.</p>

		<p class="nav-slide">Over the years, we have built up a reputation for being an industry innovator. Building on our established reputation for being master craftsmen in the use of the more traditional metal products such as zinc, copper and stainless steel, we have over the past ten years extended our skills and expertise to cover the whole of the external building envelope, to the highest of specifications.</p>

		<p class="nav-slide">Experience and detailed market knowledge of customer needs, supplier skills, legislative and environmental drivers and the emergence of new technologies have all contributed to our current market standing.</p>

		<P class="nav-slide">We are well known for our outstanding customer support; safe working practices, technical knowledge and quality workmanship all of which have been achieved through constant investment in training and certification of all personnel. We have a proven track record of working with all of the major UK developers to complete projects on time and on budget over and over again.</p>

		<ul class="big-list">

			<?php

				query_posts(array('post_parent' => 11, 'post_type' => 'page'));

				while (have_posts()) {
					the_post();

			?>

			<li class="nav-slide"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></li>

			<?php

				}

				wp_reset_query();

			?>

		</ul>

	</div>

	<div id="services" class="nav-content">

		<p class="nav-slide">Etiam aliquet cursus nibh. Phasellus vitae justo vel arcu tincidunt rutrum eleifend sit amet nisi. Ut a lectus faucibus, sagittis orci et, consequat dui. Ut finibus, dolor eget hendrerit euismod, odio quam fringilla.</p>

		<div class="three-col">
		<?php

		$args = array( 'post_type' => 'services' );
		$loop = new WP_Query( $args );

		if ($loop->have_posts()) {

		  while ($loop->have_posts()) {

		    $loop->the_post();

		    ?>

		    <div class="global-image-cell nav-slide">

		    	<div class="image">
		    		<img src="<?php the_field('icon') ?>" alt="">
		    	</div>

		    	<span>
		    		<h4><?php the_title(); ?></h4>

		    		<p><?php the_excerpt() ?></p>
		    	</span>

		    </div>

		<?php

			}

		}

		wp_reset_postdata(); // reset to the original page data

		?>
		</div>

	</div>

	<div id="sectors" class="nav-content">

		<p class="nav-slide">Etiam aliquet cursus nibh. Phasellus vitae justo vel arcu tincidunt rutrum eleifend sit amet nisi. Ut a lectus faucibus, sagittis orci et, consequat dui. Ut finibus, dolor eget hendrerit euismod, odio quam fringilla.</p>

		<div class="three-col">

			<?php

			$args = array( 'post_type' => 'sectors' );
			$loop = new WP_Query( $args );

			if ($loop->have_posts()) {

			  while ($loop->have_posts()) {

			    $loop->the_post();

			    ?>

			    <a class="global-image-cell nav-slide" href="<?php echo get_permalink(); ?>">

			    	<div class="image">
			    		<img src="<?php the_field('sector_icon') ?>" alt="">
			    	</div>

			    	<span>
			    		<h4><?php the_title(); ?></h4>

			    		<p><?php the_excerpt() ?></p>
			    	</span>

			    </a>

			<?php

				}

			}

			wp_reset_postdata(); // reset to the original page data

			?>

		</div>

	</div>

	<div id="products" class="nav-content">

		<p class="nav-slide">Vestibulum elementum tellus ac dui hendrerit sagittis. Nullam condimentum lacus vitae massa tincidunt efficitur. Integer ornare commodo nisi et elementum.</p>

		<ul class="big-list">
		<?php

			$args = array('post_type' => 'products');

			$loop = new WP_Query($args);

			if ($loop->have_posts()) {

			  while ($loop->have_posts()) {

			    $loop->the_post();
			    /* loop markup here using the_content() etc */

		?>

		    <li class="nav-slide"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></li>

		<?php

			}

		}

		wp_reset_postdata(); // reset to the original page data

		?>
		</ul>

	</div>

</div>