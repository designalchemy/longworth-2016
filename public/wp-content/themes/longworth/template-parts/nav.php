<nav>

    <div class="container">

        <div class="mobile-nav-icon"><div class="mobile-nav-burger"></div></div>

        <a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri (); ?>/img/logo.png" alt=""></a>

        <span class="nav-list">

            <a href="#about-us">About us</a>
            <a href="#services">Services</a>
            <a href="#sectors">Sectors</a>
            <a href="#products">Products</a>
            <a href="<?php echo get_home_url(); ?>/projects">Projects</a>
            <a href="<?php echo get_home_url(); ?>/news">News</a>
            <a href="<?php echo get_home_url(); ?>/contact">Contact</a>

        </span>

    </div>

</nav>
