<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Longworth
 */

get_header(); ?>

<?php
if (have_posts()) {
	while (have_posts()) {
			the_post();
?>

<section class="big-background parallax-window" data-parallax="scroll" data-image-src="<?php echo the_post_thumbnail_url(); ?>">

    <!-- <img src="img/background/leafy.png"> -->

    <div class="container">

        <h1 class="fatty fade-out-scoll"><?php the_title(); ?></h1>

    </div>

</section>

<?php

	} // end while
} // end if

get_footer();
