<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Longworth
 */

?>

		</main>

        <footer>

        	<div class="container">

            	<div class="four-col">

	            	<div>

		            	<span class="footer-line"></span>

	            		<p><a href="<?php echo get_home_url(); ?>/supplier-enquiry/">Suppliers enquires</a></p>
	            		<p><a href="<?php echo get_home_url(); ?>/privacy-policy/">Privacy Policy</a></p>
	            		<p><a href="<?php echo get_home_url(); ?>/disclaimer/">Disclaimer</a></p>

	            	</div>

	            	<div>

		            	<span class="footer-line"></span>

	            		<address>
	            			Head Office:<br>
	            			North Florida Road<br>
	            			Haydock Industrial Estate<br>
	            			Haydock, St Helends, WA11 9ub
	            		</address>

	            	</div>

	            	<div>

		            	<span class="footer-line blue"></span>

	            		<p>Telephone <a href="tel:08456341370">0845 634 1370</a></p>

	            		<p>Email <a href="mailto:info@longworth-uk.com">info@longworth-uk.com</a></p>

	            	</div>

	            	<div>

		            	<span class="footer-line"></span>

	            		<a href="https://twitter.com/longworthuk" class="fa fa-twitter-square fa-2x"></a>

	            		<a href="https://www.linkedin.com/company/longworth-building-services-ltd" class="fa fa-linkedin-square fa-2x"></a>

	            		<a href="https://www.instagram.com/longworth_insta/?hl=en" class="fa fa-instagram fa-2x"></a>

	            		<a href="https://uk.pinterest.com/longworths/" class="fa fa-pinterest-square fa-2x"></a>

	            	</div>

	            </div>

            </div>

        </footer>

        <script src="https://code.jquery.com/jquery-2.2.3.min.js" integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo=" crossorigin="anonymous"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/scripts.js"></script>

    </body>

</html>