<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */


if ($_SERVER["SERVER_NAME"] == "staging.longworth-uk.com") {

	define('DB_NAME', 'longworth');

	/** MySQL database username */
	define('DB_USER', 'longworth');

	/** MySQL database password */
	define('DB_PASSWORD', '23AyybLnpMsvRTpxNUzv');

	/** MySQL hostname */
	define('DB_HOST', 'localhost');

	/** Database Charset to use in creating database tables. */
	define('DB_CHARSET', 'utf8mb4');

	/** The Database Collate type. Don't change this if in doubt. */
	define('DB_COLLATE', '');


} else {

	define('DB_NAME', 'longworth');

	/** MySQL database username */
	define('DB_USER', 'homestead');

	/** MySQL database password */
	define('DB_PASSWORD', 'secret');

	/** MySQL hostname */
	define('DB_HOST', 'localhost');

	/** Database Charset to use in creating database tables. */
	define('DB_CHARSET', 'utf8mb4');

	/** The Database Collate type. Don't change this if in doubt. */
	define('DB_COLLATE', '');

}


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'U>m&zmN5@)^rF`(4,~+^d!QHta+OJg|:PSXNEPk}~ha|)W6Rx4T6$0?/Pp$/++?C');
define('SECURE_AUTH_KEY',  'yZLGD&~Di!Nv:HMEM Zyx.HTAB!o#G:;_:.}WQdu~5PM`|2xqbxZlP@.,AK4MIp-');
define('LOGGED_IN_KEY',    'OiPa6x@?&<` X APCJt+;| lz-sc%J#}zM+;j.cyXC9yu$$D,*Z58%IYCz?]x:5U');
define('NONCE_KEY',        '++md]9F$q~&>u|*ULuM0gaD~Hw-|dq=ONN&FMY{5Rk%eTUo*%8&>9)vE<T_6~!bV');
define('AUTH_SALT',        'C0;P[Rn=,Di3N@6<~.E0CR=S w8 >SeWQ:ST-r [PA^g0zq4-olWXA- /lE)lSGA');
define('SECURE_AUTH_SALT', '{cOCga%tV|;Ug<SM{qbHc[9{bfji|F}sn/C=G!Faz%bvG|-)f}I=5+n]K6e 7nEB');
define('LOGGED_IN_SALT',   'Y2&iU}F7)E3-zWA*Q}QD%F8EY}_+IB),AuRe?KP&X$G.:Wat-vrJ=j4|QX$M_o?F');
define('NONCE_SALT',       'Dmd%V|>vx6tp~YS[+)pZ--pPXF)nIN= X>1wK|Jv~!+fH(^HnI4EkP>/I!M&6Cwd');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
