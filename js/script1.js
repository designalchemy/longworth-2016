
// gets insagtram photos and puts em on site

function getIgPhotos() {

	$('.insta-target').empty();

	$.ajax({
	    type: 'GET',
	    url: 'https://api.instagram.com/v1/users/2551744915/media/recent/?access_token=2551744915.1677ed0.6d2112ab17904022863d6b355ac55a52',
	    dataType: 'jsonp',
	    success: function (data) {

	        $.each(data.data, function(index, element) {

	        	if (index < 3) {

	        		var html = '<a target="_blank" href="' + element.link + '" class="fade-in-on-scroll" data-delay="400">' +
                            		'<div class="blue-filter">' +
                                		'<img src="' + element.images.standard_resolution.url + '" alt="">' +
		                            '</div>' +
		                        '</a>';

	        		$('.insta-target').append(html);

	        	}

	        });

	    }
	});

}

// fades in the side navigation items one by one, opens nav container + fades in the opacity filter on body

function fadeInNavItems($target) {

	// works on nav-slider class, add it anything to be slid in

	var $children = $($target).find('.nav-slide');

	$('.nav-target, .main-body').addClass('open');

	$children.each(function (i) {

		var $this = $(this);

		window.setTimeout(function () {

			$this.addClass('open');

		}, 60 * i);

	});

}

// closes the nav

function closeNav() {

	// $('.active').removeClass('active');
	// $('.open').removeClass('open');

	$('.nav-target').removeClass('open').find('.open').removeClass('open');

	$('.nav-list a').removeClass('active');

}

// fades in items on scroll, searchs by class

function animateOnScroll(elements, options) {

	// change default values

	var defaults = $.extend(options, {
	    distanceFromTop: 0.95,
	});

	// screen size - off set

	var screenSize = $(window).height() * defaults.distanceFromTop;

	// add class for each element depending on which data attr its got

	elements.each(function () {

		var $this = $(this);
		var direction = $this.data('direction') ? $this.data('direction') : 'bottom';
		var buildClass = 'fade-from-' + direction;

		$this.addClass(buildClass);

	});

	// on scroll check if its gone past the limit

	$(document).scroll(function () {

	    var scrollAmmount = $(document).scrollTop();

	    elements.each(function () {

	    	var $this = $(this);
	    	var thisOffset = (screenSize - ($this.offset().top - $this.height())) * -1;

	    	// * -1 to make it a positive number

	    	if (scrollAmmount >= thisOffset) {

	    		var delay = $this.data('delay') ? $this.data('delay'): 0;

	    		// default to 0

	    		window.setTimeout(function() {

	    			$this.removeClass('fade-from-left fade-from-right fade-from-bottom');

	    		}, delay);

	    	}

	    });

	}).scroll();

}

// slide show function

function playSlideShow(element, options) {

	var defaults = $.extend(options, {
	    slideDelay: 6000,
	    sliderEasingTimer: 1000
	});

	var $slides = element.children('.slider-cell');
	var $textContainer = element.children('slider-text-container');
	var numberOfSlides = $slides.length;

	var numberOfElemets = $slides.length - 1;
	var slideToActive = -1; // negative as it adds one to start with
	var nextElement;

	setUpOnPageLoad(numberOfSlides);

	var $counterCell = $('.slider-counter').find('.slider-counter-cell'); // after the set up so it can find the elements

	// for (i = 0; i < numberOfSlides; i++) {

	// 	$('.slider-counter .container').append(html);

	// }

	window.setInterval(function () {

		// set current and next slide as int's

		slideToActive++;

		if (slideToActive > numberOfElemets) {
			slideToActive = 0;
		}

		// next element

		nextElement = slideToActive + 1;

		if (nextElement === numberOfElemets + 1) {

			nextElement = 0;
		}

		// color nav active

		$('.slider-counter-cell').removeClass('active');
		$($counterCell[nextElement]).addClass('active');

		// set container height

		//$('.slider-container').delay(500).animate({'height': $($slides[nextElement]).find('img').height()}, 100);

		// preform nav item move

		moveSlide($slides[slideToActive], $slides[nextElement], defaults);

	}, defaults.slideDelay);

}

function setUpOnPageLoad(numberOfSlides) {

	var html = '<div class="slider-counter-cell"></div>';

	for (i = 0; i < numberOfSlides; i++) {

		$('.slider-counter .container').append(html);

	}

	window.setTimeout(function() {

		$(window).resize(function() {

			// bump to stop on page render issue

			$('.slider-container').css({'height': $('.slider-cell').eq(0).find('img').height()}, 100)

		}).resize();

	}, 50);

	$('.slider-counter-cell').eq(0).addClass('active');

	var cells = $('.slider-cell');

	cells.eq(0).css({'z-index': '9', 'top': '0'}).addClass('active-slide');
	cells.eq(1).css({'z-index': '8'});

}

function moveSlide(element, nextElement, options) {

	var $this = $(element);
	var $nextSlide = $(nextElement);

	var thisText = $this.find('h1').text();

	// sort z-index so the next one is allways behind the current one & in correct top pos

	$nextSlide.css({'z-index': '8', 'top': '0'});
	$this.css({'z-index': '9'});

	// slide up

	$this.delay(500).animate({
    	top: '-2000px'
  	},{
	  	duration: options.sliderEasingTimer,
	  	easing: 'swing',
	});

	$('.slider-cell').removeClass('active-slide');
  	$nextSlide.addClass('active-slide');

  	// slide text up then move to negative and slide back to 0 from below

	$('.slider-text-container').find('h1').each(function () {

		var $this = $(this);

		$this.animate({
		    top: '-500px'
		}, {
		  	duration: options.sliderEasingTimer * 0.8,
		  	easing: 'swing',
			complete: function () {
		  		// call back on slide up

		  		// set text for fixed container from slide h1's

		  		var textOne = $nextSlide.find('h1').eq(0).text();
		  		var textTwo = $nextSlide.find('h1').eq(1).text();

		  		// set text below to slide up

		  		$('.first-text-target').text(textOne).css('top', 250);
		  		$('.second-text-target').text(textTwo).css('top', 250);

		  		// close the text cell animation

			}

	  	});

  		$(this).delay(options.sliderEasingTimer * 0.01).animate({
    		top: '0'
  		},{
		  	duration: options.sliderEasingTimer * 0.8,
		  	easing: 'swing',
	  	});

	});

}

//replace default a action to use animations

function replaceNavigation(url) {

	closeNav();

	window.setTimeout(function() {

		$('.loader-container').fadeIn();

	}, 150);

	window.setTimeout(function() {

		window.location.href = url;

	}, 400);

}

$(document).ready(function () {

	$('.loader-container').fadeOut();

	// click function to open sub nav or pages

	$('.nav-list a').click(function (e) {

		e.preventDefault();

		var $this = $(this);
		var href = $this.attr('href');
		var isPesduoLink;

		if (href.indexOf('#') > -1) {
			isPesduoLink = $(href).length;
		}

		if (!$this.hasClass('active') && isPesduoLink) {

			// if current nav isnt open + the target is a fake link

			closeNav();
			$this.addClass('active');
			fadeInNavItems(href);

		} else if (!isPesduoLink) {

			// if the link is a real link

			replaceNavigation(href);

		} else {

			// otherwise close the nav

			closeNav();

		}

	});


	$('a').not('.nav-list a').click(function (e) {

		e.preventDefault();

		replaceNavigation($(this).attr('href'));

	})

	// open close mobile nav

	$('.mobile-nav-icon').click(function () {

		closeNav();

		$('.nav-list, .filter').toggleClass('active');

		$('.main-body').toggleClass('open');

	});

	// x on the nav menu

	$('.js-close-nav').click(function (e) {

		e.preventDefault();

		closeNav();

		if (!$('.nav-list').hasClass('active')) {

			$('.main-body').removeClass('open');

		}

	});

	// fade out slowly thing

	$(document).scroll(function () {

	    $('.fade-out-scoll').each(function () {

	    	var $this = $(this);

	    	var offset = $this.offset().top;

	    	var scrollAmmount = ((offset - $(window).scrollTop()) / offset).toFixed(2) ;

	    	var other = (($(window).scrollTop() ) * 0.5).toFixed(2) ;

	    	$this.css({'opacity': scrollAmmount, 'transform': 'translate3d(0,' + other + 'px, 0)', position: 'relative'});

	    });

	});

	animateOnScroll($('.fade-in-on-scroll'));

	playSlideShow($('.slider-container'));

	getIgPhotos();

	$('.bxslider').bxSlider({ });

	// dirty hack to fix footer padding, could be done manually

	$(document).resize(function () {

		$('body').css('margin-bottom', $('footer').outerHeight());

	}).resize();

});
